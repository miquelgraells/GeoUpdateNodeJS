var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/route/:_id', function(req, res, next) {
	var _id = req.params._id;	
	res.render('route', { _id: _id });
});

router.get('/app', function(req, res, next) {	
	res.render('app');
});

router.get('/about', function(req, res, next) {	
	res.render('about');
});

module.exports = router;
