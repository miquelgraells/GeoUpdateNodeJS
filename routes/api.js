var express = require('express');
ObjectID = require('mongodb').ObjectID;
var fs = require('fs');
var router = express.Router();

var n = 0;

/**
 * @api {get} /api/allroutes/ Get all routes
 * @apiName Get all routes
 * @apiGroup API
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *      [
 *         {"_id":"592f07a86aec270011df9066","startRoute":{"lng":2.2101599,"lat":41.5296313},"name":"Gallecs 1","statistics":{"dateStart":"29/04/2017 10:04","dateEnd":"29/04/2017 10:54","distance":"5.14"}},
 *          {"_id":"592f07ed6aec270011df9067","startRoute":{"lng":2.1961383,"lat":41.5628167},"name":"Gallecs 2","statistics":{"dateStart":"29/04/2017 10:55","dateEnd":"29/04/2017 13:18","distance":"10.17"}}
 *        ]
 *     }.
 *
 */
router.get('/allroutes/', function(req, res) {		
	req.db.collection('routes').find({},{'name':1,'startRoute':1,'statistics':1}).toArray(function(e, results){
    	if (e) return next(e)
    	results.error = 0;
    	res.jsonp(results)
  	});		
});

/**
 * @api {get} /api/route/:id/ Get route data
 * @apiName Get route data
 * @apiGroup API
 * 
 * @apiParam {Number} id route unique ID.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *        {
 *          "_id":"592f07ed6aec270011df9067",
 *          "coor":[{"lng":2.1961383,"lat":41.5628167},{"lng":2.1961383,"lat":41.5628167}],
 *            "images":[{"name":"14962544452.jpg","lat":41.5438517,"lng":2.1990483},
 *            "name":"Gallecs 2",
 *            "statistics":{
 *                "dateStart":"29/04/2017 10:55",
 *                "dateEnd":"29/04/2017 13:18",
 *                "distance":"10.17"
 *            }
 *        }
 *
 */
router.get('/route/:id', function(req, res) {
	var id = req.params.id		
	req.db.collection('routes').findOne({_id: ObjectID(id)}, function(err, result) {
	    res.jsonp(result)	    
	});
});

/**
 * @api {delete} /api/removeRouteById/ Remove route 
 * @apiName Remove route data
 * @apiGroup API
 * 
 * @apiParam {Number} id route unique ID.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *
 */
router.delete('/removeRouteById/', function(req, res) {		
    var idRoute = req.query.idRoute;
    console.log("idRoute ----> " + idRoute);
    // removing images of route
    req.db.collection('routes').findOne({'_id': ObjectID(idRoute)},function(err,result){
        console.log("first");
        if (err) throw err;        
        if (result.images) {            
            var images = result.images;        
            for (var i = 0; i < images.length; i++) {            
                var name = images[i].name;            
                fs.unlinkSync("../imagesRoutes/"+name);            
            }        
        }
        // once all images of route has been deled, then we can delete the route collection
        req.db.collection('routes').remove({'_id': ObjectID(idRoute)});    
        res.status(200).send({'status':200});
    }); 
});

/**
 * @api {post} /insertroute/ Insert route
 * @apiName Post route data
 * @apiGroup API
 * 
 * @apiParamExample {json} Request-Example:
 *        { 
 *          "coor":[{"lng":2.1961383,"lat":41.5628167},{"lng":2.1961383,"lat":41.5628167}],
 *            "images":[{"image": "string64" ,"lat":41.5438517,"lng":2.1990483},
 *            "name":"Gallecs 2",
 *            "statistics":{
 *                "dateStart":"29/04/2017 10:55",
 *                "dateEnd":"29/04/2017 13:18",
 *                "distance":"10.17"
 *            }
 *        }
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *        {
 *          "_id":"592f07ed6aec270011df9067" 
 *        } 
 *
 */
router.post('/insertroute/', function(req, res) {		
	var route = JSON.parse(req.body.coor);
    var startRoute = JSON.parse(req.body.startRoute);
    var routeName = req.body.name;
    var statistics = JSON.parse(req.body.statistics);    
    statistics.distance =  statistics.distance.toFixed(2);
    
    if(req.body.images){
        var images = JSON.parse(req.body.images);                

        var imagesArr = new Array();
        for(var i = 0; i < images.length; i++){            
            var image = images[i];
            var lat = image.lat;
            var lng = image.lng;            
            var imageName = Math.floor(Date.now() / 1000);         
            n++;        
            var imageName = ""+imageName + n + '.jpg';
            var path = "../imagesRoutes/"+imageName;                      
            imagesArr.push({'name':imageName,'lat':lat,'lng':lng});
            base64_decode(image.image, path);        
        }
    }		
    
    var routeObj = {'coor':route,'images':imagesArr,'startRoute':startRoute, 'name' :routeName, 'statistics' : statistics}

	req.db.collection('routes').insert(routeObj, function(err, coor){
        console.log("id _____ " + coor["ops"][0]["_id"]);        
        if (err) throw err;
        res.status(200).send({'status' : 200,'id' : coor["ops"][0]["_id"]});
    }); 
	
});

function base64_decode(base64str, file) {
    console.log("---inside64");        
    // create buffer object from base64 encoded string, it is important to tell the constructor that the string is base64 encoded
    var bitmap = new Buffer(base64str, 'base64');    
    // write buffer to file
    fs.writeFileSync(file, bitmap);
    console.log('******** File created from base64 encoded string ********');
}

/**
 * @api {get} /imagesRoutes/:file Get image
 * @apiName Get image
 * @apiGroup API
 * 
 * @apiParam {String} :file image name
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *        {
 *            image/jpg
 *        }
 *
 */
router.get('/imagesRoutes/:file', function (req, res){
    file = req.params.file;    
    var img = fs.readFileSync("../imagesRoutes/"+file);
    //var img = fs.readFileSync(file);
    res.writeHead(200, {'Content-Type': 'image/jpg' });
    res.end(img, 'binary'); 
});

module.exports = router;
