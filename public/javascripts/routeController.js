// Controller for each route

var App = angular.module('app', []);

App.controller('routeController', ["$scope", "$http", function controller($scope,$http) {

	// to know if user wants the markers to be showed or hided
	hidemarkers = false;	
	// array to hold all the markers at the map
	markersArr = [];	
	// array of name of images of images
	$scope.routeImages = [];
	// array of name of images but not in the scope so it can be acceed at the marker listener
	routeImages2 = [];
	// route name
	$scope.routeName = "Lorem ipsum";
	// route distance
	$scope.routeDistance = "0.0";
	// route date start
	$scope.routeDateStart = "0.0";
	// route date end
	$scope.routeDateEnd = "0.0";
	// to show on image of marker or to show all images
	$scope.showMarkerImage = true;
	// name of the marker
	$scope.markerImage = "";	

	$scope.init = function (_id) {
		// hide image marker
		$("#ImageMarkerContainer").hide();							

		// request to get route data		
		$http({
			method : "GET",      
			url : "/api/route/"+_id,
		}).then(function mySucces(response) {
			console.log(JSON.stringify(response.data));
			// get images name
			$scope.routeImages = response.data.images;
			// get the images name but not saved in the scope so it can be acceed at the marker listener
			routeImages2 = response.data.images;
			// route name
			$scope.routeName = response.data.name;
			// route distance
			$scope.routeDistance = response.data.statistics.distance;
			// route date start
			$scope.routeDateStart = response.data.statistics.dateStart;
			// route date end
			$scope.routeDateEnd = response.data.statistics.dateEnd;
			// route coordinates
			var coor = response.data.coor;

			// initzialise map
	   		map = new google.maps.Map(document.getElementById('mapRoute'), {
				center: new google.maps.LatLng(41.345048, 2.129210),
				zoom: 15
			}); 

	   		//------------Button to SHOW/HIDE image markers------------------
			// Create the DIV to hold the control and call the showHideMarkerButon()
	        // constructor passing in this DIV.
	        var showHideMarkers = document.createElement('div');
	        var centerControl = new showHideMarkerButon(showHideMarkers, map);
	        showHideMarkers.index = 1;
	        map.controls[google.maps.ControlPosition.TOP_CENTER].push(showHideMarkers);
	        
			// show route on map	
			var routePolyLine = new google.maps.Polyline({
          		path: coor,
          		geodesic: true,
          		strokeColor: '#FF0000',
          		strokeOpacity: 1.0,
          		strokeWeight: 2
        	});
        	routePolyLine.setMap(map);

        	// center map
        	var bounds = new google.maps.LatLngBounds();    		
    		for (var i = 0; i < coor.length ; i++){
        		bounds.extend(coor[i]);
    		}
    		map.fitBounds(bounds);

    		// set markers for the images
    		if ($scope.routeImages) {    		
    			var scope2 = $scope;
	    		for (var i = 0; i < $scope.routeImages.length; i++) {
	    			(function (i) {

		    			var position = {
							'lat' : $scope.routeImages[i].lat,
							'lng' : $scope.routeImages[i].lng	
						}

		    			marker = new google.maps.Marker({
								position: position,
								map: map,
								icon: '../images/picture.png'
							});

						marker.addListener('click', function() {			    							  				  	
						  	window.location = "../api/imagesRoutes/" + $scope.routeImages[i].name;
			        	});

			        	marker.addListener('mouseover', function() {		
			        		$("#listImagesRoutes").hide();	        		
			        		$("#ImageMarkerContainer").show();
			        		$('#ImageMarker').attr("src", "/api/imagesRoutes/" + routeImages2[i].name);    						
						});

						marker.addListener('mouseout', function() {
							$("#listImagesRoutes").show();
							$("#ImageMarkerContainer").hide();							    						
						});

						markersArr.push(marker);

		        	})(i);
	    		}
    		}
    		// set marker for start route
    		marker = new google.maps.Marker({
						position: {
							"lat" : coor[0].lat,
							"lng" : coor[0].lng
						},
						map: map,
						icon: '../images/start_point.png'
					});    		
    		// set marker for end route
			marker = new google.maps.Marker({
						position: {
							"lat" : coor[coor.length-1].lat,
							"lng" : coor[coor.length-1].lng
						},
						map: map,
						icon: '../images/end_point.png'
					});			
		})		
	};

	// when user does a mouseover image this function is called to show the marker
	// that coresponds to the photo
	$scope.indicateMarker = function (index) {
		var marker = markersArr[index];
		marker.setIcon('../images/pictureIndicated.png');
		markerPosition = marker.position;
		map.setCenter(markerPosition);
		map.setZoom(16);	
		if (hidemarkers) {
			marker.setVisible(true);
		}
	}

	// when the user does a mouseleave this function is called to set the normal marker
	$scope.resetMarker = function (index) {
		var marker = markersArr[index];
		marker.setIcon('../images/picture.png');
		if (hidemarkers) {
			marker.setVisible(false);
		}
	}

	// this function creates a buton to be added at the map and allows 
	// to show or hide the markers
	function showHideMarkerButon(controlDiv, map) {

        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginBottom = '22px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'SHOW / HIDE MARKERS';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(25,25,25)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '16px';
        controlText.style.lineHeight = '38px';
        controlText.style.paddingLeft = '5px';
        controlText.style.paddingRight = '5px';
        controlText.innerHTML = 'SHOW / HIDE MARKERS';
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', function() {
        	hidemarkers = !hidemarkers ? true : false;
        	for (var i = markersArr.length - 1; i >= 0; i--) {
        		markersArr[i].setVisible(markersArr[i].getVisible() ? false : true);        		
        	}
        });

      }
}]);