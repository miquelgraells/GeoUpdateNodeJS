// Controller for the main page wher all the routes are showed

var App = angular.module('app', []);

App.controller('mainPageController', ["$scope", "$http", function controller($scope,$http) {

	// arry to hold the markers
	var markersArr = [];
	// array to hold all the routes
	$scope.allrouteArr = new Array;

	// init controller
	$scope.init = function () {
		// initzialise map
	   	map = new google.maps.Map(document.getElementById('map'), {
			center: new google.maps.LatLng(41.345048, 2.129210),
			zoom: 15
		}); 
		// get all routes
		$http({
			method : "GET",      
			url : "/api/allroutes",
		}).then(function mySucces(response) {
			$scope.allrouteArr = response.data;
			// bounds of the map
			bounds  = new google.maps.LatLngBounds();
			// add marker on map
			for (var i = 0; i < $scope.allrouteArr.length; i++) {
				(function (i) {
					var position = {
						'lat' : $scope.allrouteArr[i].startRoute.lat,
						'lng' : $scope.allrouteArr[i].startRoute.lng	
					}

					map.panTo(position);

					marker = new google.maps.Marker({
								title: $scope.allrouteArr[i].name + ' \n' + $scope.allrouteArr[i].statistics.distance + ' Km',
								position: position,
								map: map,
								icon: '../images/startRoute.png'
							});

					// whe user clics a merker whe open that route
				    marker.addListener('click', function() {			    	
				    	$scope.openRoute(i);	          			
	        		});

	        		markersArr.push(marker);

	        		loc = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
					bounds.extend(loc);  

        		})(i);
			}
			// auto-zoom
			map.fitBounds(bounds);      
			// auto-center 
			map.panToBounds(bounds);    		

		});			
	};		

	// when user selcts one route we get the id of the route and redirect 
	// the user to the page of the route
	$scope.openRoute = function (i) {
		var id = $scope.allrouteArr[i]._id;	
		$http({
			method : "GET",      
			url : "/route/"+id,						
		}).success(function(data) {              										
			window.location = "/route/"+id						
		})
	}
			
	// this function is called when user does a mouseover on the lateral info of the route 
	$scope.indicateMarker = function (index) {
		var marker = markersArr[index];
		markerPosition = marker.position;
		map.setCenter(markerPosition);
		map.setZoom(12);
		marker.setIcon('../images/startRoute_green.png');
	}

	// this function is called when user does a mouseleave on of the lateral info of the route
	$scope.resetMarker = function (index) {
		var marker = markersArr[index];
		marker.setIcon('../images/startRoute.png');
	}

}]);
